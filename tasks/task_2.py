from django.db.models import Count
from .models import A, B

def task_2():
	task_2_1 = B.objects.filter(a=None).count()
	task_2_2 = A.objects.annotate(b_count=Count('b')).all()
	task_2_3 = A.objects.filter(b__flag=True) | A.objects.filter(b=None)
	task_2_3.all()

	return {'task_2_1': task_2_1, 'task_2_2': task_2_2, 'task_2_3': task_2_3}