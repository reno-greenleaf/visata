from django.test import TestCase
from .task_1 import distribute_costs

class Test(TestCase):
	def test_costs(self):
		inpt = [{'m3': 1.323, 'cost': 113.67}, {'m3': 0.561, 'cost': 213.5}, {'m3': 2.312, 'cost': 317.22}, {'m3': 1.567, 'cost': 522.3}, {'m3': 0.997, 'cost': 325.14}]
		# inpt = [{'cost': 999.99}]
		m3_out = [1.1, 0.313, 0.467, 1.2, 0.951, 1.113, 1.363]
		result = distribute_costs(inpt, m3_out)

		original_cost = sum([p['cost'] for p in inpt])
		resulting_cost = sum([p['cost'] for p in result])
		self.assertEqual(original_cost, resulting_cost)