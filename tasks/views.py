from .models import B
from .serializers import BSerializer
from rest_framework.generics import ListAPIView

class Task3(ListAPIView):
	queryset = B.objects.all()
	serializer_class = BSerializer