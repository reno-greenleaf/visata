from rest_framework.serializers import ModelSerializer, CharField, IntegerField
from .models import B

class BSerializer(ModelSerializer):
	a_name = CharField(source='a.name', default='')
	a_id = IntegerField(read_only=True, source='a.id')

	class Meta:
		model = B
		fields = ['id', 'text', 'a_name', 'a_id']