def distribute_costs(original_packages, resulting_packages_m3):
	total_cost = sum([package['cost'] for package in original_packages])
	total_m3 = sum(resulting_packages_m3)
	m3_cost = total_cost / total_m3 # cost of 1 m3 of resulting package
	result = []

	for m3 in resulting_packages_m3:
		package = {
			'm3': m3,
			'cost': round(m3 * m3_cost, 2)
		}
		result.append(package)

	# take care of difference caused by rounding
	resulting_total_cost = sum([package['cost'] for package in result])

	if resulting_total_cost != total_cost:
		if resulting_total_cost > total_cost:
			minimum = -0.01
		else:
			minimum = 0.01

		for package in result:
			package['cost'] += minimum

			if sum([package['cost'] for package in result]) == total_cost:
				break

	return result